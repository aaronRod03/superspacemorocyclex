﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Message : MonoBehaviour {
	[SerializeField]
TextMeshPro text;
	// Use this for initialization
	public void ShowText(string text, float duration){
		this.text.text = text;
		StopAllCoroutines();
		StartCoroutine(MoveUp());
		StartCoroutine(Disable(duration));
	}
	IEnumerator MoveUp(){
		while(true){
			transform.position+=Vector3.up*Time.deltaTime*5;
			yield return null;
		}
	}
	IEnumerator Disable(float duration){
		yield return new WaitForSeconds(duration);
		StopAllCoroutines();
		gameObject.SetActive(false);
	}
}
