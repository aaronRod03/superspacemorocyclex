﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbit : MonoBehaviour, ISelectable {
	[SerializeField]
	Color color1;
	[SerializeField]
	Color color2;
	[SerializeField]
	Color selectedColor1;
	[SerializeField]
	Color selectedColor2;
	SpriteRenderer spriteRenderer;
	bool isSelected;
    public Planet linkedToPlanet;
	// Use this for initialization
	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer>();
		StartCoroutine(ChangeColor());
	}


	IEnumerator ChangeColor(){
		float value = 0;
		Color targetColor1 = color1;
		Color targetColor2 = color2;
		while(true){
			if (isSelected)
			{
				targetColor1 = selectedColor1;
				targetColor2 = selectedColor2;
			}
			else
			{
				targetColor1 = color1;
				targetColor2 = color2;
			}
			value = Mathf.Sin(Time.time);
			spriteRenderer.color = Color.Lerp(targetColor1, targetColor2, value);
			yield return null;
		}
	}

	public void Select(bool selected)
	{
		isSelected = selected;
	}
}
