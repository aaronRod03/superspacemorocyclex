﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using TMPro;
public class UI_Ship : MonoBehaviour {
	Ship ship;
	[SerializeField]
	Transform resourcesIndicatorsParent;
    [SerializeField]
    Text txtDuration;
    [SerializeField]
    SinglePool resourcesPool;
    [SerializeField]
    Transform mainCanvas;
    [SerializeField]
    RectTransform resourceTarget;
    [SerializeField]
    AnimationCurve resourceCurve;
    [SerializeField]
    float curveAmplitude;
    [SerializeField]
    GameObject mainUI;
    [SerializeField]
    GameObject pausedUI;
    [SerializeField]
    GameObject startingUI;
    [SerializeField]
    GameObject FinishedUI;
    [SerializeField]
    GameObject infoUI;
    [SerializeField]
    Image musicButton;
    [SerializeField]
    Sprite[] musicSprites = new Sprite[2];
    [SerializeField]
    TextMeshProUGUI txtScore;
        public List<Image> comboImages;

    public List<Image> resourceImages;
    public List<PlanetResources> currentCombination;
	// Use this for initialization
	void Start () {
        ship = GameManager.Instance.player;
		ship.OnPropertyChanged += UpdateProperties;
        resourcesPool.CreatePool(3,mainCanvas);
        FinishedUI.SetActive(false);
        mainUI.SetActive(false);
        pausedUI.SetActive(false);
        startingUI.SetActive(true);
        infoUI.SetActive(false);
        CreateNewCombo(false);
        UpdateMusicButton();
	}
	
	// Update is called once per frame
	void Update () {
        if(!GameManager.Instance.gameFinished)
       txtDuration.text = GetTimeToString();
	}
    string GetTimeToString(){
         int seconds = ship.timeAlive.Second;
        int minutes = ship.timeAlive.Minute;
        double milliseconds = ship.timeAlive.Millisecond;
        string timeInString =string.Format("{0:00}",minutes)+":"+ string.Format("{0:00}",seconds)+":"+ 
        string.Format("{0:00}",milliseconds/10);
        return timeInString;
    }
	void UpdateProperties(PlanetResources resource)
    {
                StartCoroutine(ShowNewResource(resource));
    }
    void AddResource(){
                int dex = 0;
         foreach (PlanetResources item in ship.planetResources)
                {
                    resourceImages[dex].gameObject.SetActive(true);
                    resourceImages[dex].color = PlanetsLibrary.Instance.getPlanetByResource(item).color;
                    dex++;
                }
                CheckPlayerCombo(ship.planetResources.ToList());
    }
    IEnumerator ShowNewResource(PlanetResources planet){
        GameObject newResource = resourcesPool.getObject(true);
        RectTransform rectTransform = newResource.GetComponent<RectTransform>();
        
            newResource.transform.localScale = new Vector2(1,1);
        rectTransform.position = Camera.main.WorldToScreenPoint( ship.transform.position);
        newResource.GetComponent<Image>().color = PlanetsLibrary.Instance.getPlanetByResource(planet).color;
        float elapsed=0;
        float amplitude= UnityEngine.Random.Range(-curveAmplitude,curveAmplitude);
        while(elapsed<.7f){
            elapsed+=Time.deltaTime*3;
            rectTransform.position = Vector2.Lerp(rectTransform.position,resourceTarget.position,elapsed);
            rectTransform.position += Vector3.right*amplitude*resourceCurve.Evaluate(elapsed/.7f);
            yield return null;
        }
        float scale = 1;
        while(scale>.3f){
            scale-=Time.deltaTime*5;
            newResource.transform.localScale = new Vector2(scale,scale);
            yield return null;
        }
        newResource.gameObject.SetActive(false);
        AddResource();
    }
    void CheckPlayerCombo(List<PlanetResources> playerCombo)
    {
        bool comboSucces = true;
        if (playerCombo.Count>=currentCombination.Count)
        {
            for (int i = 0; i < currentCombination.Count; i++)
            {
                if (playerCombo[i] != currentCombination[i])
                {
                    comboSucces = false;
                    break;
                }
            }
            if (comboSucces)
            {
                GameManager.Instance.map.RestoreRandomPlanet();
				ship.InvokeComboCompleted();
                foreach (Image item in resourceImages)
                {
                    item.color = Color.white;
                    item.gameObject.SetActive(false);
                }
                CreateNewCombo(false);
            }
        }
    }

    public void CreateNewCombo(bool sendedByEnemy)
    {
        List<Planet> alivePlanets = GameManager.Instance.map.currentPlanets;
        List<PlanetResources> resourcesInCurrentMap = new List<PlanetResources>();
        foreach (Planet item in alivePlanets)
        {
            if(item.isActiveAndEnabled){
                if (item.planetData.currentResource!= PlanetResources.Empty)
                {
                    resourcesInCurrentMap.Add(item.planetData.currentResource);
                }
            }
        }
        resourcesInCurrentMap = resourcesInCurrentMap.Distinct().ToList();
        if(sendedByEnemy){
            foreach(PlanetResources resource in currentCombination){
                if(!resourcesInCurrentMap.Contains(resource))
                 break;
            }
            return;
        }
        currentCombination = new List<PlanetResources>();
        foreach (Image item in comboImages)
        {
            item.color = Color.white;
            item.transform.parent.gameObject.SetActive(false);
        }
        if (resourcesInCurrentMap.Count>0)
        {
            int comboLength = 1;//UnityEngine.Random.Range(2, 5);
            for (int i = 0; i < comboLength; i++)
            {
                int resourceIndex = UnityEngine.Random.Range(0, resourcesInCurrentMap.Count);
                currentCombination.Add(resourcesInCurrentMap[resourceIndex]);
                comboImages[i].transform.parent.gameObject.SetActive(true);
                comboImages[i].color = PlanetsLibrary.Instance.getPlanetByResource(resourcesInCurrentMap[resourceIndex]).color;
            }
        }
        
    }
    void UpdateMusicButton(){
        musicButton.sprite = AudioManager.Instance.playMusic?musicSprites[0]:musicSprites[1];
    }
    public void SetMusicValue(){
        AudioManager.Instance.PlayMusic();
        UpdateMusicButton();
    }
    public void ShowFinishedUI(){
        FinishedUI.SetActive(true);
        mainUI.SetActive(false);
        txtScore.text = GetTimeToString();
    }
    public void ShowInfo(){
        infoUI.SetActive(true);
    }
    public void StartGame(){
        GameManager.Instance.StartGame();
        startingUI.SetActive(false);
        mainUI.SetActive(true);
    }
    public void PauseGame(){
        GameManager.Instance.Pause();
        if(GameManager.Instance.isPaused){
            pausedUI.SetActive(true);
            mainUI.SetActive(false);
        }else{
            pausedUI.SetActive(false);
            mainUI.SetActive(true);
        }
    }
    public void OpenURL(string url){
        Application.OpenURL(url);
    }
    public void Restart(){
        GameManager.Instance.Restart();
    }

}
