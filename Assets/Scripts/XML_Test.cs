﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using UnityEngine;
using System.Linq;

public class XML_Test : MonoBehaviour
{
    public int numberOfPlanets;
    public static XML_Test ins;
    public List<MapLayout> entries = new List<MapLayout>();
    public List<Planet> currentPlanets;
    public GameObject prefab;
    int currentIndex = 0;
    public string currentLayoutName;
    public int currentLayoutToLoad = 0;
    public Vector2 minPosition = new Vector2(-100000,-100000);
	public Vector2 maxPosition = new Vector2(Mathf.Infinity,Mathf.Infinity);   
    public Planet leftPlanet;
    public Planet topPlanet;
    public Planet bottomPlanet;
    public Planet rightPlanet;
    public ParticleSystem planetRestoration;

    public void CreateMap(){
        ins = this;
        LoadItems(); 
        LoadMap();
        List<Planet> planets = currentPlanets;
		minPosition = planets[0].transform.position;
		maxPosition = planets[0].transform.position;
        leftPlanet = rightPlanet = topPlanet = bottomPlanet = planets[0];

		foreach(Planet planet in planets){
			Vector3 minPlanetPosition = planet.transform.position-Vector3.one*planet.orbitRadius;
			Vector3 maxPlanetPosition = planet.transform.position+Vector3.one*planet.orbitRadius;
			if(minPlanetPosition.x<minPosition.x){
				minPosition.x = minPlanetPosition.x;
                leftPlanet = planet;
            }
			if(minPlanetPosition.y<minPosition.y){
				minPosition.y = minPlanetPosition.y;
                bottomPlanet = planet;
            }
			if(maxPlanetPosition.x>maxPosition.x){
				maxPosition.x = maxPlanetPosition.x;
                rightPlanet = planet;
            }
			if(maxPlanetPosition.y>maxPosition.y){
				maxPosition.y = maxPlanetPosition.y;
                topPlanet = planet;
            }
		}
    }

    private void Start()
    {
        planetRestoration.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            SaveItems();
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadMap();
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            MapLayout layout = new MapLayout();
            currentPlanets = FindObjectsOfType<Planet>().ToList();
            foreach (Planet item in currentPlanets)
            {
                PlanetClass newPlanet = new PlanetClass();
                newPlanet.orbitSize = item.planetData.orbitSize;
                newPlanet.planetPosition = item.transform.position;
                newPlanet.currentResource = item.planetData.currentResource;
                layout.planets.Add(newPlanet);
                Debug.Log(item.planetData.orbitSize);
            }
            layout.playerStartPosition = layout.planets[0];
            layout.layoutName = currentLayoutName;
            entries.Add(layout);
        }
    }

    public void RestoreRandomPlanet(){
        Planet planet = currentPlanets.Find(x=>x.isActiveAndEnabled==false);
        if(planet){
            Debug.Log("Restaurado");
            planetRestoration.gameObject.SetActive(true);
            planetRestoration.gameObject.transform.position = planet.transform.position;
            ParticleSystem.MainModule main = planetRestoration.main;
            main.startColor = PlanetsLibrary.Instance.getPlanetByResource(planet.planetData.currentResource).color;
            planetRestoration.Play();
            planet.gameObject.SetActive(true);
            planet.Restore();
        }
    }

    public void SpawnPlanet(int index)
    {
        PlanetClass data = entries[currentLayoutToLoad].planets[index];
        Planet planet = Instantiate(prefab).GetComponent<Planet>();
        planet.SetPlanet(data);
		planet.name = data.currentResource.ToString();
        currentPlanets.Add(planet);
    }

    public void SaveItems()
    {
        XmlSerializer serializar = new XmlSerializer(typeof(List<MapLayout>));
        FileStream stream = new FileStream(Application.dataPath + "/Resources/MapLayouts.xml", FileMode.Truncate);
        serializar.Serialize(stream, entries);
        stream.Close();
#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
#endif
    }

    public void LoadItems()
    {
        XmlSerializer serializar = new XmlSerializer(typeof(List<MapLayout>));
     
		TextAsset file = Resources.Load("MapLayouts") as TextAsset;
        using (var sr = new StringReader(file.text))
        {
			entries = (List<MapLayout>)serializar.Deserialize(sr);
            sr.Close();
        } // end using

    }

    public void LoadMap()
    {
        LoadItems();
        if (entries.Count > 0)
        {
            currentPlanets.Clear();
            //currentLayoutToLoad = Random.Range(0, entries.Count);
            for (int i = 0; i < entries[currentLayoutToLoad].planets.Count; i++)
            {
                SpawnPlanet(i);
            }
        }
    }
}

[System.Serializable]
public class MapLayout
{
    public string layoutName;
    public PlanetClass playerStartPosition;
    public List<PlanetClass> planets;

    public MapLayout()
    {
        playerStartPosition = new PlanetClass();
        planets= new List<PlanetClass>();
    }
}

[System.Serializable]
public class PlanetClass
{
    public Vector3 planetPosition;
    public float orbitSize;
    public PlanetResources currentResource;

    public PlanetClass()
    {
        currentResource = PlanetResources.Empty;

    }

    public PlanetClass(Vector3 _pos, float _size)
    {
        planetPosition = _pos;
        orbitSize = _size;
    }
}


[System.Serializable]
public enum PlanetResources { Fenridio, Ricardio, Aronio, Felipesio, Othesio, Angelonio, Jossfato, Jocelynio, Escuderio, Zaragozio, Itzelsio, Emilirio, Joycenico, Isabelio, Empty}
