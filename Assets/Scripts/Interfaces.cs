﻿public interface IInteractible{
	void Interact();
}
public interface ISelectable{
	void Select(bool selected);
}