﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour{
	bool isSelected = false;
	public float orbitRadius = 1;
	[SerializeField]
	GameObject prefabOrbit;
	public Orbit orbit;
    public PlanetClass planetData;
    bool dataLoaded = false;
    public ParticleSystem[] planetParticles;
    public void Start()
    {
        if (!dataLoaded)
        {
            planetData.planetPosition = transform.position;
            orbitRadius = planetData.orbitSize;
        }
        MeshRenderer rend = GetComponent<MeshRenderer>();
		rend.material.color = PlanetsLibrary.Instance.getPlanetByResource(planetData.currentResource).color;
        int particleAura = Random.Range(0, 3);
        switch (particleAura)
        {
            case 0:
            case 1:
                ParticleSystem aura = Instantiate(planetParticles[particleAura], gameObject.transform).GetComponent<ParticleSystem>();
                var main = aura.main;
                main.startColor = PlanetsLibrary.Instance.getPlanetByResource(planetData.currentResource).color;
                break;
            case 2:
                break;
            default:
                break;
        }
    }

    public void SetPlanet(PlanetClass data)
    {
        dataLoaded = true;
        planetData = data;
		transform.position = data.planetPosition+Vector3.forward*10;
        orbitRadius = data.orbitSize;
		orbit = Instantiate(prefabOrbit).GetComponent<Orbit>();
        orbit.linkedToPlanet = this;
        orbit.transform.position = transform.position;
        orbit.transform.localScale = Vector3.one * orbitRadius * 2;
        
    }

    public void Select(bool selected)
	{
		if (selected != isSelected)
		{
			isSelected = selected;
			orbit.Select(isSelected);
			if (selected)
			{

			}
			else
			{
			}
		}
	}

    public void Destroy(){
        gameObject.SetActive(false);
        orbit.gameObject.SetActive(false);
    }

    public void Restore(){
        orbit.gameObject.SetActive(true);
    }
}
