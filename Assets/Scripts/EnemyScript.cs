﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    public Transform target;
    public float rotationSpeed;
    public float movementSpeed;

   public void SetValues(Ship player)
    {
        target = player.transform;
    }

    void Update()
    {
        if (target!=null)
        {
            //Debug.DrawLine(transform.position, transform.up * 100, Color.red);
            Vector2 transition = Vector2.MoveTowards(transform.position, target.position, movementSpeed * Time.deltaTime);
            transform.position = new Vector3(transition.x, transition.y, 20);
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
		Debug.Log("collision");
        Planet hitPlanet = other.GetComponent<Planet>();
        if (hitPlanet)
        {
            movementSpeed += 0.2f;
            transform.localScale += new Vector3(0.2f, 0.2f, 0.2f);
            hitPlanet.Destroy();
            GameManager.Instance.ui.CreateNewCombo(true);
            GameManager.Instance.camera.CreateShake(new CameraController.ShakeInfo(.7f,.1f));
            return;
        }
        Ship hitPlayer = other.GetComponent<Ship>();
        if (hitPlayer)
        {
            GameManager.Instance.Finish();
            hitPlayer.StopAllCoroutines();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        
    }
}
