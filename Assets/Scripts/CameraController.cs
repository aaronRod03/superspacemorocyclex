﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
	public struct ShakeInfo
    {
        public ShakeInfo(float _duration, float _magnitude)
        {
            duration = _duration;
            magnitude = _magnitude;
        }

        public float duration;
        public float magnitude;
    }

    Vector3 cameraPosition;
	[SerializeField]
	Camera camera;
	XML_Test map;
	bool isShaking;
	// Use this for initialization
	public void SetValues () {
		
		map = GameManager.Instance.map;
		Vector2 minPosition = map.minPosition;
		Vector2 maxPosition = map.maxPosition;

		 camera.transform.position = (minPosition+maxPosition)*.5f;
		 float verticalSize = maxPosition.y-minPosition.y;
		 float horizontalSize = maxPosition.x-minPosition.x;
		 camera.orthographicSize = verticalSize*.7f;
		 cameraPosition = camera.transform.position;
	}
	

	void Update(){
		if(!isShaking){
			camera.transform.position = Vector3.Lerp(camera.transform.position,cameraPosition,Time.unscaledDeltaTime);
		}
	}
    public void CreateShake(ShakeInfo info)
    {
        StopAllCoroutines();
        StartCoroutine(Shake(info));
		isShaking = true;
    }

    IEnumerator Shake(ShakeInfo info)
    {
        float elapsed = 0.0f;
        float duration = info.duration;
        float magnitude = info.magnitude;
        while (elapsed < duration)
        {
            elapsed += Time.deltaTime;

            float percentComplete = elapsed / duration;
            float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);

            // map value to [-1, 1]
            float x = Random.value * 2.0f - 1.0f;
            float y = Random.value * 2.0f - 1.0f;
            x *= magnitude * damper;
            y *= magnitude * damper;
            camera.transform.position +=  new Vector3(x, y, 0);

            yield return null;
        }
		isShaking =false;
    }
}
