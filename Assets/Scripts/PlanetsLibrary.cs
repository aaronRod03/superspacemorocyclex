﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetsLibrary : MonoBehaviour {
	public static PlanetsLibrary Instance;
	[SerializeField]
	List<PlanetDefinition> planetDefinitions = new List<PlanetDefinition>();
	private void Awake()
	{
		if(Instance)
		{
			Destroy(this);
			return;
		}
		Instance = this;
	}
	public PlanetDefinition getPlanetByResource(PlanetResources planetResources){
		PlanetDefinition planetDefinition = planetDefinitions.Find(x => x.planetResources == planetResources);
		return planetDefinition;
	}
}
[System.Serializable]
public class PlanetDefinition{
	public PlanetResources planetResources;
	public Color color;
}