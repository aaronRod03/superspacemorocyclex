﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionManager : MonoBehaviour {
	public ISelectable currentSelectedObject;
	[SerializeField]
	Ship ship;

	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0)&&GameManager.Instance.gameStarted &&!GameManager.Instance.gameFinished && !GameManager.Instance.isPaused)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100))
            {
				if (hit.collider != null && hit.collider.gameObject.activeInHierarchy)
				{

					ISelectable selectable = hit.collider.gameObject.GetComponent<ISelectable>();
					if (selectable != null)
					{
						if (selectable != currentSelectedObject)
						{
							if(currentSelectedObject!=null)
								currentSelectedObject.Select(false);
							currentSelectedObject = selectable;
							currentSelectedObject.Select(true);
							ship.SetNewTarget(hit.collider.gameObject);
						}
					}
				}
            }
        }
	}

}
