﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour {
	public static GameManager Instance;
	public XML_Test map;
	public CameraController camera;
	public Ship player;
	public UI_Ship ui;
	public SelectionManager selection;
	[SerializeField]
	EnemyScript enemy;
	public bool gameFinished;
	public bool gameStarted;
	public bool isPaused;
	void Awake()
	{
		if(Instance){
			Destroy(this);
			return;
		}
		Instance = this;
		Time.timeScale = 1;
		map.CreateMap();
		camera.SetValues();
        player.transform.position = new Vector3(map.minPosition.x, map.minPosition.y, player.transform.position.z);
        Application.targetFrameRate = 30;
	}
	
	// Update is called once per frame
	public void Pause(){
		isPaused=!isPaused;
		if(isPaused){
			Time.timeScale = 0;
		}else{
			Time.timeScale = 1;
		}
	}
	public void Finish(){
		gameFinished = true;
		camera.CreateShake(new CameraController.ShakeInfo(1.3f,.3f));
		ui.ShowFinishedUI();
	}
	public void StartGame(){
		Time.timeScale = 1;
		enemy.SetValues(player);
		player.SetNewTarget(map.leftPlanet.orbit.gameObject);
		map.leftPlanet.Select(true);
		selection.currentSelectedObject = map.leftPlanet.orbit;
		gameStarted = true;
	}
	public void Restart(){
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
}
