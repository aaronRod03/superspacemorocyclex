﻿using UnityEngine;

public class ObjectInPool : MonoBehaviour {
    Quaternion originalState;
    private Pool myPool;
    void Awake()
    {
        originalState = transform.rotation;
    }

    public void addToPool()
    {
        if (myPool!=null)
        myPool.ReceiveObject(this.gameObject);
    }

    public void setPool(Pool pool)
    {
        myPool = pool;
    }

    void OnDisable()
    {
        transform.rotation = originalState;
        addToPool();
    }
}
