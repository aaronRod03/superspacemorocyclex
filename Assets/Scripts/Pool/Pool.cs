﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class Pool
{
    protected Transform parent;
    public delegate void CatchedObject(GameObject obj);
    event CatchedObject OnReceiveObject;
    protected List<GameObject> objectsInPool = new List<GameObject>();
    public virtual void ReceiveObject(GameObject obj)
    {
        objectsInPool.Add(obj);
        if (OnReceiveObject != null)
            OnReceiveObject(obj);
    }
    public void AddReceiveObjectListener(CatchedObject listener)
    {
        OnReceiveObject += listener;
    }
    public void DisableAll()
    {
        for(int i = 0; i < objectsInPool.Count; i++)
        {
            objectsInPool[i].SetActive(false);
        }
    }
}
[System.Serializable]
public class SinglePool:Pool
{
    public GameObject objectToPool;

    public SinglePool()
    {

    }
    public SinglePool(int startSize)
    {
        for (int x = 0; x < startSize; x++)
        {
            GameObject newObject = createObject();
            ObjectInPool poolled = newObject.GetComponent<ObjectInPool>();
            poolled.setPool(this);
            newObject.SetActive(false);
        }
    }

    public SinglePool(GameObject objectToPool,int startSize)
    {
        this.objectToPool = objectToPool;
        for(int x = 0; x < startSize; x++)
        {
            GameObject newObject = createObject();
            ObjectInPool poolled = newObject.GetComponent<ObjectInPool>();
            poolled.setPool(this);
            newObject.SetActive(false);
        }
    }

    public void CreatePool(int startSize, Transform parent = null)
    {
        this.parent = parent;
        for (int x = 0; x < startSize; x++)
        {
            GameObject newObject = createObject();
            newObject.transform.SetParent(parent, true);
            ObjectInPool poolled = newObject.GetComponent<ObjectInPool>();
            poolled.setPool(this);
            newObject.SetActive(false);
        }
    }

    public void AddElement()
    {
        GameObject newObject = createObject();
        ObjectInPool poolled = newObject.GetComponent<ObjectInPool>();
        poolled.setPool(this);
        newObject.SetActive(false);
    }

    GameObject createObject()
    {
        GameObject newObject =  GameObject.Instantiate(objectToPool);
        newObject.transform.SetParent(parent, false);
        ObjectInPool poolled = newObject.GetComponent<ObjectInPool>();
        if (!poolled)
            poolled = newObject.AddComponent<ObjectInPool>();
        poolled.setPool(this);
        return newObject;
    }

    public GameObject getObject(bool create)
    {
        if (objectsInPool.Count > 0)
        {
            GameObject obj = objectsInPool[0];
            objectsInPool.RemoveAt(0);
            obj.SetActive(true);
            return obj;
        }
        if (create)
            return createObject();
        return null;
    }

    public override void ReceiveObject(GameObject obj)
    {
            base.ReceiveObject(obj);
    }
}
[System.Serializable]
public class MultiplePool : Pool
{
    [SerializeField]
    public List<GameObject> objectsToPool;

    public MultiplePool()
    {
        objectsToPool = new List<GameObject>();
    }

    public void addObjectToPool(GameObject objectToPool)
    {
        objectsToPool.Add(objectToPool);
    }
    public void createPool( int startSize, Transform parent= null)
    {
        this.parent = parent;
        for (int elements = 0; elements < objectsToPool.Count; elements++)
        {
            for (int x = 0; x < startSize; x++)
            {
                GameObject newObject = createObject(elements);
                newObject.transform.parent = parent;
                ObjectInPool poolled = newObject.GetComponent<ObjectInPool>();
                poolled.setPool(this);
                newObject.SetActive(false);
            }
        }

        for(int x = 0; x < objectsInPool.Count; x++)
        {
            int random = Random.Range(0, objectsInPool.Count);
            GameObject temp = objectsInPool[x];
            objectsInPool[x] = objectsInPool[random];
            objectsInPool[random] = temp;
        }
    }

    GameObject createObject(int index)
    {

        GameObject newObject = GameObject.Instantiate(objectsToPool[index]);
        newObject.transform.parent = parent;
        ObjectInPool poolled = newObject.GetComponent<ObjectInPool>();
        if (!poolled)
            poolled = newObject.AddComponent<ObjectInPool>();
        poolled.setPool(this);
        return newObject;
    }

    public GameObject getObject(bool create)
    {
        if (objectsInPool.Count > 0)
        {
            GameObject obj = objectsInPool[0];
            objectsInPool.RemoveAt(0);
            obj.SetActive(true);
            return obj;
        }
        if (create)
            return createObject(Random.Range(0,objectsToPool.Count));
        return null;
    }
}