﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {
	public static AudioManager Instance;
	[SerializeField]
	AudioSource music;
	public bool playMusic;
	// Use this for initialization
	void Awake(){
		if(Instance){
			Destroy(this);
			return;
		}
		Instance = this;
		if(PlayerPrefs.HasKey("Music")){
			if(PlayerPrefs.GetInt("Music")==1)
			PlayMusic();
		}else{
			PlayerPrefs.SetInt("Music",1);
			PlayMusic();
		}
	}
	public void PlayMusic(){
		playMusic = !playMusic;
		PlayerPrefs.SetInt("Music",playMusic?1:0);
		if(playMusic){
			music.Play();
		}else{
			music.Stop();
		}
	}
	
}
