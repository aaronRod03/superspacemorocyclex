﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour {
	public delegate void ComboCompleted();
	public event ComboCompleted OnComboCompleted;
	public DateTime timeAlive;
	    public enum Property { Elements };
    public delegate void PropertyChanged(PlanetResources property);
    public event PropertyChanged OnPropertyChanged;
	[SerializeField]
	public float speed = 10;
	[SerializeField]
	public float orbitSpeed= 10;
	[SerializeField]
	AnimationCurve speedCurve;
	[SerializeField]
	SinglePool messagePool;
    public Queue<PlanetResources> planetResources = new Queue<PlanetResources>();

	Vector3 actualTargetPosition;
	/*public PlanetResources lastResourceObtained{
		get
        {
			return planetResources[planetResources.Count - 1];
		}
	}*/

	GameObject actualTarget;
	bool isTraveling;
	void Start(){
		timeAlive = new DateTime();
		messagePool.CreatePool(3);
        OnComboCompleted += SpeedBurst;
	}
	void Update(){
		timeAlive = timeAlive.AddMilliseconds(Time.deltaTime*1000);
	}
	public void SetNewTarget(GameObject target){
		actualTarget = target;
		actualTargetPosition = actualTarget.transform.position;
		if (!isTraveling)
		{
			isTraveling = true;
			StopAllCoroutines();
			StartCoroutine(ReachNewTarget());
		}
	}

	public void AddResource(PlanetResources resource){
		if (!isTraveling && resource != PlanetResources.Empty)
		{
            planetResources.Enqueue(resource);
            if (planetResources.Count>1)
            {
                planetResources.Dequeue();

            }
			GameObject message = messagePool.getObject(true);
			message.transform.position=transform.position;
			Message messageScript = message.GetComponent<Message>();
			messageScript.ShowText(resource.ToString(),1);
			CallEvent(resource);
		}
	}

	public void InvokeComboCompleted(){
		planetResources.Clear();
        SpeedBurst();
	}

    float originalSpeed;
    float originalOrbit;

    public void SpeedBurst()
    {
        Debug.Log("Cambiando velocidad");
        originalSpeed = speed;
        originalOrbit = orbitSpeed;
        speed = 20;
        orbitSpeed = 100;
        StartCoroutine(SpeedUpRoutine());
    }

    IEnumerator SpeedUpRoutine()
    {
        float speedTime = 3.0f;
        while (speedTime>0)
        {
            speedTime -= Time.deltaTime;
            yield return null;
        }
        speed = 10;
        orbitSpeed = 50;
    }

    public void CallEvent(PlanetResources property)
    {
        if (OnPropertyChanged != null)
            OnPropertyChanged(property);
    }

	IEnumerator ReachNewTarget()
	{
		Planet actualPlanet = actualTarget.GetComponent<Orbit>().linkedToPlanet;
		float orbitSize = actualTarget.GetComponent<Orbit>().linkedToPlanet.orbitRadius;
		float totalDistance;
		float elapsedDistance = 0;
		Vector3 direction = actualTarget.transform.position - transform.position;
		Vector3 targetPosition = actualTarget.transform.position;
		totalDistance = direction.magnitude;
		direction.Normalize();
		GameObject startingTarget = actualTarget;

		while (Vector2.Distance(transform.position, targetPosition) > orbitSize)
		{
			float nextIncrement = Time.deltaTime * speed * speedCurve.Evaluate(elapsedDistance / totalDistance);
			transform.position += direction * nextIncrement;
			elapsedDistance += nextIncrement;
			yield return null;
		}
		isTraveling = false;
		AddResource(actualPlanet.planetData.currentResource);
		if (actualTarget != startingTarget)
			SetNewTarget(actualTarget);
		else
			StartCoroutine(Orbit());
	}

	IEnumerator Orbit(){
		while(true){
			if(actualTarget == null)
				break;
			transform.RotateAround(actualTargetPosition, Vector3.forward, Time.deltaTime * orbitSpeed);
			yield return null;
		}
		GameManager.Instance.Finish();
	}
}
